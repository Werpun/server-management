import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GetServers } from '../server.actions';
import { Server, ServerListState } from '../server.state';

@Component({
  selector: 'app-server-list',
  templateUrl: './server-list.component.html',
  styleUrls: ['./server-list.component.scss']
})
export class ServerListComponent implements OnInit {
  @Select(ServerListState.getServers) servers$!: Observable<Server[]>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(new GetServers());
  }

  trackByFn(_index: number, server: Server) {
    return server.id;
  }
}
