import { Server } from './server.state';

export class GetServers {
  static readonly type = '[server] get servers]';
}

export class StartServer {
  static readonly type = '[server] start server]';
  constructor(public readonly server: Server) {}
}

export class StopServer {
  static readonly type = '[server] stop server]';
  constructor(public readonly server: Server) {}
}

export class RebootServer {
  static readonly type = '[server] reboot server]';
  constructor(public readonly server: Server) {}
}

export class UpdateServer {
  static readonly type = '[server] update server]';
  constructor(public readonly payload: { id: number; server: Server }) {}
}
