import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { RebootServer, StartServer, StopServer } from '../server.actions';
import { Server } from '../server.state';

@Component({
  selector: 'app-server-detail',
  templateUrl: './server-detail.component.html',
  styleUrls: ['./server-detail.component.scss']
})
export class ServerDetailComponent implements OnInit {
  @Input()
  server!: Server;

  constructor(private store: Store) {}

  ngOnInit(): void {}

  startServer() {
    this.store.dispatch(new StartServer(this.server));
  }

  stopServer() {
    this.store.dispatch(new StopServer(this.server));
  }

  rebootServer() {
    this.store.dispatch(new RebootServer(this.server));
  }
}
