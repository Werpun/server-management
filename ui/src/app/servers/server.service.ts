import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Server } from './server.state';

const BASE_URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  constructor(private http: HttpClient) {}

  getServers() {
    return this.http.get<Server[]>(`${BASE_URL}/servers`);
  }

  getServer(id: number) {
    return this.http.get<Server>(`${BASE_URL}/servers/${id}`);
  }

  startServer(id: number) {
    return this.http.put<Server>(`${BASE_URL}/servers/${id}/on`, {
      serverId: id
    });
  }

  stopServer(id: number) {
    return this.http.put<Server>(`${BASE_URL}/servers/${id}/off`, {
      serverId: id
    });
  }

  rebootServer(id: number) {
    return this.http.put<Server>(`${BASE_URL}/servers/${id}/reboot`, {
      serverId: id
    });
  }
  
}
