import { Action, Selector, State, StateContext } from '@ngxs/store';
import {
  GetServers,
  RebootServer,
  StartServer,
  StopServer,
  UpdateServer
} from './server.actions';
import { ServerService } from './server.service';
import { mergeMap, retryWhen, tap } from 'rxjs/operators';
import { patch, updateItem } from '@ngxs/store/operators';
import { Injectable } from '@angular/core';
import { retryStrategy } from '../operators';

export interface Server {
  id: number;
  name: string;
  status: 'ONLINE' | 'OFFLINE' | 'REBOOTING';
}

export interface ServerStateModel {
  servers: Server[];
}

@State<ServerStateModel>({
  name: 'servers',
  defaults: { servers: [] }
})
@Injectable()
export class ServerListState {
  @Selector()
  static getServers(state: ServerStateModel) {
    return state.servers;
  }

  constructor(private serverService: ServerService) {}

  @Action(GetServers)
  getServers(ctx: StateContext<ServerStateModel>) {
    return this.serverService.getServers().pipe(
      tap(servers => {
        ctx.patchState({ servers });
      })
    );
  }

  @Action(UpdateServer)
  updateServer(ctx: StateContext<ServerStateModel>, { payload }: UpdateServer) {
    ctx.setState(
      patch({
        servers: updateItem<Server>(s => s?.id === payload.id, payload.server)
      })
    );
  }

  @Action(StartServer)
  startServer(ctx: StateContext<ServerStateModel>, { server }: StartServer) {
    return this.serverService.startServer(server.id).pipe(
      tap(server => {
        ctx.dispatch(new UpdateServer({ id: server.id, server }));
      })
    );
  }

  @Action(StopServer)
  stopServer(ctx: StateContext<ServerStateModel>, { server }: StartServer) {
    return this.serverService.stopServer(server.id).pipe(
      tap(server => {
        ctx.dispatch(new UpdateServer({ id: server.id, server }));
      })
    );
  }

  @Action(RebootServer)
  rebootServer(ctx: StateContext<ServerStateModel>, { server }: StartServer) {
    return this.serverService.rebootServer(server.id).pipe(
      mergeMap(server => {
        ctx.dispatch(new UpdateServer({ id: server.id, server }));
        return this.serverService.getServer(server.id).pipe(
          tap(server => {
            if (server.status === 'REBOOTING')
              throw 'Server is still rebooting';

            ctx.dispatch(new UpdateServer({ id: server.id, server }));
          }),
          retryWhen(retryStrategy({ initialDelay: 1000, maxRetryAttempts: 10 }))
        );
      })
    );
  }
}
