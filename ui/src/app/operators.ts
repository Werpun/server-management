import { defer, Observable, throwError, timer } from 'rxjs';
import { concatMap } from 'rxjs/operators';

export interface RetryStrategyConfig {
  maxRetryAttempts: number;
  initialDelay: number;
  scalingDuration?: number;
}

export function retryStrategy(
  config: RetryStrategyConfig
): <T>(source: Observable<T>) => Observable<number> {
  const {
    maxRetryAttempts = 5,
    initialDelay = 1000,
    scalingDuration = 1
  } = config;

  return <T>(source: Observable<T>) =>
    defer(() => {
      let currAttempt = 0;
      return source.pipe(
        concatMap(attempt => {
          if (++currAttempt > maxRetryAttempts)
            return throwError(`Failed after ${maxRetryAttempts} attempts`);

          const delay = initialDelay * currAttempt * scalingDuration;
          console.log(`Attempt ${currAttempt}: retrying in ${delay}ms`);

          return timer(delay);
        })
      );
    });
}
